//Unlike arrays that use indexes, objects use properties

/*let grades = {
	firstGrading: 89.2,
	secondGrading: 98.2,
	thirdGrading: 90.1,
	forthGrading: 88.7
};*/

// An objects is created through curly brackets and key-value pairs.

/*let person = {
	firstName: "John",
	lastName: "Smith"
};*/

// firstName: 'John' - this is known as a key-value pair; firstName is the key, and "John" is the value

//the colon(;) separates the key and the value, and the comma (,) after the value separates the key-value pairs.

// the term key is also called a property of an object, and its value is found after the right side of the colon character. Property is often used than key.


//An object can also contain another object (nested object), arrays

/*let grades = {
	firstName: 'John',
	lastName: 'Smith',
	location: {
		city: 'Tokyo',
		country: 'Japan',
	}

};*/

// An object can also contain arrays

let person = {
	firstName: "John",
	lastName: "Smith",
	location: {
		city: "Tokyo",
		country: "Japan",
	},
	emails: ["john@mail.com", "johnsmith@mail.com"],
	 brushTeeth: function(){
	 	console.log(this.firstName + " has brushed his teeth")
	}
};

// We use arrays if we are talking about different groups, things, lists - however, we use objects if we are talking about properties of the same thing.

// we can also have an array of objects

/*let products = [
	{
		name: 'Soap',
		description: 'This is a soap',
		price: 10
	},
	{
		name: 'Shampoo',
		description: 'This is a shampoo',
		price: 20
	},
	{
		name: 'Toothpaste',
		description: 'This is a toothpaste',
		price: 30
	}
]*/

// Objects can also contain functions - a function inside of an object is called a method, and called a 'function' outside of an object

/*let grades = {
	firstName: 'John',
	lastName: 'Smith',
	location: {
		city: 'Tokyo',
		country: 'Japan',
	},
	emails: ['john@mail.com', 'johnsmith@mail.com'],
	fullName: function(){
		return this.firstName + ' ' + this.lastName;
	}
};*/


// strings inside an object should be declared using double quotation marks " "

/*
	ACCESSING OBJECT PROPERTIES

	Use the dot notation to access object's property's value

*/

//console.log(person.lastName)

//console.log(grades.fullName)

// accessing an array inside of an object
console.log(person.emails[0])
console.log(person.location.country)

let example = {
	prop1: [
		{
			prop2: ["cat", "dog"]
		}
	]
}
console.log(example.prop1[0].prop2[1]) // to get the value of the "dog" array

/*
	NOTE: Array is NOT a data type, it's an OBJECT. 
*/


// INITIALIZING/ ADDING & EDITING OBJECT PROPERTIES

let car = {
	manufacturerDate: 1999
}

car.name = "Honda Civic"
car.manufacturerDate = 2019

console.log(car)