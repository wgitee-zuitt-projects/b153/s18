let trainer = {}

trainer.name = "Ash Ketchum"
trainer.age = 10
trainer.pokemon = ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"]
trainer.friends = {
	hoenn: ["May", "Max"],
	kanto: ["Brock", "Misty"]
}
trainer.talk = function(){
				console.log("Pikachu! I choose you!")
}
let name = trainer.name;

console.log(trainer)

console.log("Result of dot notation: ")
console.log(name)

console.log("Result of talk method:")
trainer.talk();
